type = fancymenu_layout

layout-meta {
  identifier = join_multiplayer_screen
  render_custom_elements_behind_vanilla = true
  last_edited_time = 1717190405893
  is_enabled = true
  randommode = false
  randomgroup = 1
  randomonlyfirsttime = false
  layout_index = 0
  custom_menu_title =  
  [loading_requirement_container_meta:ffc5260d-1ad4-496f-b0d5-7924cb2202aa-1717190400024] = [groups:][instances:]
}

menu_background {
  image_path = [source:local]/config/fancymenu/assets/fon.png
  slide = false
  repeat_texture = false
  background_type = image
}

customization {
  action = backgroundoptions
  keepaspectratio = false
}

scroll_list_customization {
  preserve_scroll_list_header_footer_aspect_ratio = true
  scroll_list_header_texture = [source:local]/config/fancymenu/assets/void.png
  scroll_list_footer_texture = [source:local]/config/fancymenu/assets/void.png
  render_scroll_list_header_shadow = false
  render_scroll_list_footer_shadow = false
  show_scroll_list_header_footer_preview_in_editor = false
  repeat_scroll_list_header_texture = false
  repeat_scroll_list_footer_texture = false
}

element {
  source = [source:local]/config/fancymenu/assets/line_down.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = a77bb97b-ab30-4a93-98f6-8f0006d1c4f4-1717187291239
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = ff813be0-f75d-442f-8941-bbf2924f9812-1717188569906
  x = -110
  y = 20
  width = 310
  height = 6
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = a4ea87af-f7ab-4537-9490-e911a4c1a592-1717187291239
  [loading_requirement_container_meta:a4ea87af-f7ab-4537-9490-e911a4c1a592-1717187291239] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/line_left.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 81b13870-bf53-40ea-bb85-c811533faa93-1717187462426
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 0b39f28d-40a9-4567-9e5d-2ef69927afee-1717188201052
  x = 0
  y = -24
  width = 6
  height = 22
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = f5121281-8025-4126-b7f8-c14fa4f58891-1717187462426
  [loading_requirement_container_meta:f5121281-8025-4126-b7f8-c14fa4f58891-1717187462426] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/line_right.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 4752c26c-aca0-4e94-ab42-87e08417485e-1717187470554
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 28d32f79-e24d-43d8-b27f-596a43049d99-1717188207412
  x = 0
  y = -22
  width = 6
  height = 22
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = b0be5d48-cc77-4916-8b67-12e1f6eccc2c-1717187470554
  [loading_requirement_container_meta:b0be5d48-cc77-4916-8b67-12e1f6eccc2c-1717187470554] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/line_up.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 2c2dfe14-11df-48c7-b810-01a475b21c4e-1717187475658
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 502970
  x = -105
  y = -16
  width = 310
  height = 6
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 09339fd4-d47e-47b3-a9f1-937be11254cb-1717187475658
  [loading_requirement_container_meta:09339fd4-d47e-47b3-a9f1-937be11254cb-1717187475658] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/3.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 5367ade4-2d2d-4cb4-9f22-20711384f3ff-1717188191004
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 2c2dfe14-11df-48c7-b810-01a475b21c4e-1717187475658
  x = -6
  y = 0
  width = 6
  height = 6
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = bad4a38e-b81b-4416-812c-8966564a9a6e-1717188191004
  [loading_requirement_container_meta:bad4a38e-b81b-4416-812c-8966564a9a6e-1717188191004] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/2.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 0b39f28d-40a9-4567-9e5d-2ef69927afee-1717188201052
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = a77bb97b-ab30-4a93-98f6-8f0006d1c4f4-1717187291239
  x = -6
  y = 0
  width = 6
  height = 6
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 91b84152-937f-424e-8c38-3954ca7a6db4-1717188201052
  [loading_requirement_container_meta:91b84152-937f-424e-8c38-3954ca7a6db4-1717188201052] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/1.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 28d32f79-e24d-43d8-b27f-596a43049d99-1717188207412
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = a77bb97b-ab30-4a93-98f6-8f0006d1c4f4-1717187291239
  x = 310
  y = 0
  width = 6
  height = 6
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 309224dd-c0d7-49d6-b3c8-c2d2ff27a694-1717188207412
  [loading_requirement_container_meta:309224dd-c0d7-49d6-b3c8-c2d2ff27a694-1717188207412] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/4.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 52faf55d-de4c-47ae-ada0-cbdedd9a1b95-1717188215543
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 2c2dfe14-11df-48c7-b810-01a475b21c4e-1717187475658
  x = 310
  y = 0
  width = 6
  height = 6
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = e454c7a3-300a-48b7-9ffb-acb4d1e862be-1717188215543
  [loading_requirement_container_meta:e454c7a3-300a-48b7-9ffb-acb4d1e862be-1717188215543] = [groups:][instances:]
}

element {
  interactable = true
  source = Виберіть сервер:
  source_mode = direct
  shadow = true
  scale = 1.0
  base_color = #FFFFFFFF
  text_border = 2
  line_spacing = 2
  enable_scrolling = true
  auto_line_wrapping = true
  remove_html_breaks = true
  code_block_single_color = #737373FF
  code_block_multi_color = #565656FF
  headline_line_color = #A9A9A9FF
  separation_line_color = #A9A9A9FF
  hyperlink_color = #0771FCFF
  quote_color = #818181FF
  quote_indent = 8.0
  quote_italic = false
  bullet_list_dot_color = #A9A9A9FF
  bullet_list_indent = 8.0
  bullet_list_spacing = 3.0
  parse_markdown = true
  element_type = text_v2
  instance_identifier = ff813be0-f75d-442f-8941-bbf2924f9812-1717188569906
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = top-centered
  x = -46
  y = 4
  width = 93
  height = 19
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 0c536144-4120-4583-bb12-d34d661715ba-1717188569906
  [loading_requirement_container_meta:0c536144-4120-4583-bb12-d34d661715ba-1717188569906] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/line_right.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 439f90a3-a1e5-4ad4-ad11-8024d5cc0c13-1717189806533
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 52faf55d-de4c-47ae-ada0-cbdedd9a1b95-1717188215543
  x = 0
  y = 71
  width = 6
  height = 56
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = b0be5d48-cc77-4916-8b67-12e1f6eccc2c-1717187470554
  [loading_requirement_container_meta:b0be5d48-cc77-4916-8b67-12e1f6eccc2c-1717187470554] = [groups:][instances:]
}

element {
  source = [source:local]/config/fancymenu/assets/line_left.png
  repeat_texture = false
  nine_slice_texture = false
  nine_slice_texture_border_x = 5
  nine_slice_texture_border_y = 5
  element_type = image
  instance_identifier = 2661351b-5d6b-4df0-878a-c66d51bcee93-1717189865547
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 5367ade4-2d2d-4cb4-9f22-20711384f3ff-1717188191004
  x = 0
  y = 17
  width = 6
  height = 56
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = f5121281-8025-4126-b7f8-c14fa4f58891-1717187462426
  [loading_requirement_container_meta:f5121281-8025-4126-b7f8-c14fa4f58891-1717187462426] = [groups:][instances:]
}

vanilla_button {
  button_element_executable_block_identifier = b4c20880-b4e2-4071-ba09-0e645c53f6b6-1717183969699
  [executable_block:b4c20880-b4e2-4071-ba09-0e645c53f6b6-1717183969699][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 580970
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 502970
  x = 104
  y = 0
  width = 100
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 0cfe17c6-f059-49ec-a06d-90eab2f8507a-1717183969699
  [loading_requirement_container_meta:0cfe17c6-f059-49ec-a06d-90eab2f8507a-1717183969699] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 102820ec-074d-405f-b296-d404540aa1fb-1717183969699
  [executable_block:102820ec-074d-405f-b296-d404540aa1fb-1717183969699][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 346946
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 502970
  x = -104
  y = 0
  width = 100
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = bdb4d0c4-58d5-45e4-b7ce-70c3780e636a-1717183969699
  [loading_requirement_container_meta:bdb4d0c4-58d5-45e4-b7ce-70c3780e636a-1717183969699] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = e1f572ca-aaaf-4f2d-b464-11a3f1c5300f-1717183969699
  [executable_block:e1f572ca-aaaf-4f2d-b464-11a3f1c5300f-1717183969699][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 658946
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 499
  y = 304
  width = 20
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 8782a25b-030f-4637-8aab-efcd1d72e3ba-1717183969699
  [loading_requirement_container_meta:8782a25b-030f-4637-8aab-efcd1d72e3ba-1717183969699] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 8b82f871-2a23-44d2-943e-e8d697c63fc1-1717183969699
  [executable_block:8b82f871-2a23-44d2-943e-e8d697c63fc1-1717183969699][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 450946
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 291
  y = 304
  width = 100
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = e04d62d9-0fb1-4f8d-9399-7a2aeb7792f8-1717183969699
  [loading_requirement_container_meta:e04d62d9-0fb1-4f8d-9399-7a2aeb7792f8-1717183969699] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = e3fa1c18-c60e-4bf9-9f97-cd488bdbb662-1717183969699
  [executable_block:e3fa1c18-c60e-4bf9-9f97-cd488bdbb662-1717183969699][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 346970
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 187
  y = 328
  width = 74
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 33e8179d-cc6a-4efa-b677-b1aca48b24b7-1717183969699
  [loading_requirement_container_meta:33e8179d-cc6a-4efa-b677-b1aca48b24b7-1717183969699] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = e7e6f2df-4c12-44c1-8109-33d95187a9d1-1717183969699
  [executable_block:e7e6f2df-4c12-44c1-8109-33d95187a9d1-1717183969699][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 502970
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = bottom-centered
  x = -51
  y = -45
  width = 100
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = ee2b660b-307c-4c20-8ac9-d8da6bc1120e-1717183969699
  [loading_requirement_container_meta:ee2b660b-307c-4c20-8ac9-d8da6bc1120e-1717183969699] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 8a2c945d-e994-43a2-aa46-96fc21df4d69-1717183969699
  [executable_block:8a2c945d-e994-43a2-aa46-96fc21df4d69-1717183969699][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 554946
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 502970
  x = 54
  y = 22
  width = 100
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = c088ac99-39d2-4a9b-a673-76275188258a-1717183969699
  [loading_requirement_container_meta:c088ac99-39d2-4a9b-a673-76275188258a-1717183969699] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 354c410b-8a47-4e8a-be2f-1490179911ba-1717183969699
  [executable_block:354c410b-8a47-4e8a-be2f-1490179911ba-1717183969699][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 424970
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = element
  anchor_point_element = 502970
  x = -50
  y = 22
  width = 100
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = c418dae8-ffc8-4ae7-9596-3b9ef583cb43-1717183969699
  [loading_requirement_container_meta:c418dae8-ffc8-4ae7-9596-3b9ef583cb43-1717183969699] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = f85d58dd-02ef-4566-b360-98a20549a972-1717183969699
  [executable_block:f85d58dd-02ef-4566-b360-98a20549a972-1717183969699][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 658970
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 499
  y = 328
  width = 20
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 996aad6b-887c-4b52-b9d9-81e0d8a70491-1717183969699
  [loading_requirement_container_meta:996aad6b-887c-4b52-b9d9-81e0d8a70491-1717183969699] = [groups:][instances:]
  is_hidden = true
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

