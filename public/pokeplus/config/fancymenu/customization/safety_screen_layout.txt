type = fancymenu_layout

layout-meta {
  identifier = safety_screen
  render_custom_elements_behind_vanilla = false
  last_edited_time = 1717183905296
  is_enabled = true
  randommode = false
  randomgroup = 1
  randomonlyfirsttime = false
  layout_index = 0
  [loading_requirement_container_meta:5ec48e0b-cc5b-4f28-93ad-22b6b6cee641-1717183827510] = [groups:][instances:]
}

menu_background {
  image_path = [source:local]/config/fancymenu/assets/fon.png
  slide = false
  repeat_texture = false
  background_type = image
}

customization {
  action = backgroundoptions
  keepaspectratio = false
}

scroll_list_customization {
  preserve_scroll_list_header_footer_aspect_ratio = true
  render_scroll_list_header_shadow = true
  render_scroll_list_footer_shadow = true
  show_scroll_list_header_footer_preview_in_editor = false
  repeat_scroll_list_header_texture = false
  repeat_scroll_list_footer_texture = false
}

vanilla_button {
  button_element_executable_block_identifier = cd44ab6a-9511-43a0-b72d-da7b228f3caf-1717183827510
  [executable_block:cd44ab6a-9511-43a0-b72d-da7b228f3caf-1717183827510][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 505154
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 346
  y = 172
  width = 150
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 5a701c5d-cde1-4313-9c5f-434bee4b78e2-1717183827510
  [loading_requirement_container_meta:5a701c5d-cde1-4313-9c5f-434bee4b78e2-1717183827510] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 2e9028e3-31ff-42ab-bbda-db4520db1850-1717183827510
  [executable_block:2e9028e3-31ff-42ab-bbda-db4520db1850-1717183827510][type:generic] = [executables:]
  backgroundnormal = [source:local]/config/fancymenu/assets/button.png
  backgroundhovered = [source:local]/config/fancymenu/assets/button_h.png
  background_texture_inactive = [source:local]/config/fancymenu/assets/button_h.png
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 345154
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 186
  y = 172
  width = 150
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 2cd6f2bc-1d2b-482a-8d33-6f3c63a2d73e-1717183827510
  [loading_requirement_container_meta:2cd6f2bc-1d2b-482a-8d33-6f3c63a2d73e-1717183827510] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = dd2b9378-7c5d-4042-9182-5b0375202216-1717183827510
  [executable_block:dd2b9378-7c5d-4042-9182-5b0375202216-1717183827510][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 415130
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 256
  y = 148
  width = 178
  height = 20
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = bc2ba229-c0b3-44b5-ab63-e844af53c420-1717183827510
  [loading_requirement_container_meta:bc2ba229-c0b3-44b5-ab63-e844af53c420-1717183827510] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

